'''
Created on Feb 9, 2014

@author: brian
'''

import sys


class Board(object):
    '''
    The class to handle the main logic about the board, etc
    '''
    STATE_UNKNOWN = 0
    STATE_ON = 1
    STATE_OFF = 2


    def __init__(self, width, height):
        '''
        Constructor
        '''
        self.width = width;
        self.height = height;
        self.cells = [self.STATE_UNKNOWN]*(width*height)
        self.rows = [0]*height
        self.solved_rows = set()
        self.cols = [0]*width
        self.solved_cols = set()
        self.solved = False
        
    def initialize(self):
        
        def validate_values(vals, length):
            n = len(vals)
            req = sum(vals) + n - 1
            return req <= length
        
        c = 0
        while c < self.width:
            sys.stdout.write("Column %d: " % (c+1))
            vals = sys.stdin.readline().strip()
            self.cols[c] = [int(v) for v in vals.split(" ") if int(v) > 0] if len(vals) > 0 else [0]
            if len(self.cols[c]) == 0: self.cols[c] = [0]
            # Our column values have to fit within the specified height
            if not validate_values(self.cols[c], self.height):
                print "Column is too big, try again"
            else:
                c += 1
            
        r = 0
        while r < self.height:
            sys.stdout.write("Row %d: " % (r+1))
            vals = sys.stdin.readline().strip()
            self.rows[r] = [int(v) for v in vals.split(" ") if int(v) > 0] if len(vals) > 0 else [0]
            if len(self.rows[r]) == 0: self.rows[r] = [0]
            # Our row values have to fit within the specified width
            if not validate_values(self.rows[r], self.width):
                print "Row is too big, try again"
            else:
                r += 1
                
        print ""
                    
    def solve(self, draw_interval = 0):
        step = 0
        while not self.solved:
            for row_num in range(0, self.height):
                if row_num in self.solved_rows:
                    continue
                row = self.rows[row_num]
                states = self._row_states(row_num)
                self._calculate_overlaps(row, states)
                self._calculate_on_states(row, states)
                solved = self._calculate_off_states(row, states)
                self._save_states(states)
                if solved:
                    self.solved_rows.add(row_num)
                    
                #print "Step %d, row %d" % (step, row_num)
                #self.draw(with_headers=True, verbose=True)
                
            for col_num in range(0, self.width):
                if col_num in self.solved_cols:
                    continue
                col = self.cols[col_num]
                states = self._col_states(col_num)
                self._calculate_overlaps(col, states)
                self._calculate_on_states(col, states, -1 if step != 2 else 0)
                solved = self._calculate_off_states(col, states)
                self._save_states(states)
                if solved:
                    self.solved_cols.add(col_num)
                
                #print "Step %d, col %d" % (step, col_num)
                #self.draw(with_headers=True, verbose=True)

            # TODO add in something to detect if anything change during a given phase
            # - if nothing changed during the phase, make a random guess and keep going
            
            step += 1
                
            #"""@@
            if step > 20:
                break
            #"""##
        
    
    def _save_states(self, states):
        for state in states:
            self.cells[state[0]] = state[1]
            
    def _col_states(self, col):
        return [[i, self.cells[i]] for i in range(col, self.width*self.height, self.width)]
    
    def _row_states(self, row):
        start = row*self.width
        return [[i, self.cells[i]] for i in range(start, start+self.width)]
    
    
    def _calculate_off_states(self, vals, states):
        ## Calculate if all the values are satisfied for this row ##
        p = 0
        vals_satisfied = True
        for val in vals:
            # Skip to the first value that is on
            while p < len(states) and states[p][1] != self.STATE_ON:
                p += 1
            # Count them while they're on
            onlen = 0
            while p < len(states) and states[p][1] == self.STATE_ON:
                onlen += 1
                p += 1
            # If we don't have enough to satisfy this value, fail
            if onlen != val:
                vals_satisfied = False
                break
            
        # All values are satisfied, turn off everything that is not on
        if vals_satisfied:
            for p in range(0, len(states)):
                if states[p][1] != self.STATE_ON:
                    states[p][1] = self.STATE_OFF
            return True

        
        ## Calculate if any of the state/end gaps are too small for their respective values ##
        
        # Forward and backwards, calculate the gaps and turn them off if they can't fit the value
        # Also, calculate if a value is satisfied by the next set of cells that are on
        for i in range(0, 2):
            p = 0
            vp = 0
            inc = 1
            
            # Second pass is in reverse
            if i == 1:
                p = len(states) - 1
                vp = len(vals) - 1
                inc = -1

            in_gap = False # state flag that we are working on a gap
            in_value = False # state flag that we are working on a value
            value_len = 0 # the length of the value we just measured
            gap_start = p
            gap_len = 0
            while p >= 0 and p < len(states) and vp >= 0 and vp < len(vals):
                if states[p][1] == self.STATE_OFF:
                    # If we had a gap followed by a value
                    if in_value and in_gap:
                        # If the value is not the right size of the gap is large enough to contain it
                        # The we don't handle this case here and we will iterate again 
                        if value_len != vals[vp] or gap_len >= vals[vp]:
                            break
                    # If we just processed a gap and it's too small, turn it all off
                    if in_gap and gap_len < vals[vp]:
                        in_gap = False
                        for i in range(0, gap_len):
                            states[gap_start+i*inc][1] = self.STATE_OFF
                    # Otherwise the value could fit and we're moving on to our next piece of logic
                    else:
                        break
                    # If we just processed a value, move to the next value
                    if in_value:
                        vp += inc
                        in_value = False
                    # Set these to their default state
                    gap_len = value_len = 0
                elif states[p][1] == self.STATE_ON:
                    in_value = True
                    value_len += 1
                elif states[p][1] == self.STATE_UNKNOWN:
                    # If we were in a value, we don't handle this here
                    if in_value:
                        break
                    # If we are starting a new gap
                    if not in_gap:
                        gap_start = p
                        in_gap = True
                        
                    gap_len += 1
                    
                # Move to our next cell
                p += inc
        
        # Phase 3 (hard) - turn cells are that are impossible on this row
        # eg. because of the values left, there is no way for the cells to be turned on 
        #   given the cells that are already selected
        #   2 2 | _ # _ X X _ # _ (the cells marked X are always invalid if the cells # are on)
        
        # Calculate the number of cell blocks that are turned on
        blocks = []
        in_block = False
        block_start = 0
        for p in range(0, len(states)):
            if states[p][1] == self.STATE_ON and not in_block:
                block_start = p
                in_block = True
            elif states[p][1] != self.STATE_ON:
                if in_block:
                    blocks.append((block_start, p))
                in_block = False
        # We ended the loop in a block
        if in_block:
            blocks.append((block_start, p))
                
        # If we have the same number of block as we do values
        empty_start = 0 # location where our empty region starts
        if len(blocks) == len(vals):
            for vp in range(0, len(vals)):
                bstart,bend = blocks[vp]
                blen = bend - bstart
                delta = vals[vp] - blen # How many spaces on each side of the value we need
                # If this value is the size it needs to be for this value
                if delta == 0:
                    if bstart > 0: # turn the left side off if it's not the first cell
                        states[bstart-1][1] = self.STATE_OFF
                    # turn the left side off
                    if bend < len(states):
                        states[bend][1] = self.STATE_OFF # turn the left side off
                        
                empty_end = bstart - delta
                if empty_end > empty_start:
                    for i in range(empty_start, empty_end):
                        states[i][1] = self.STATE_OFF
                empty_start = bend + delta
                
        
        # Calculate the length of each block of cells that are on
        #  If a given block can only match one value (eg 2 5 2 and the block is 4 then it can only match 5),
        #  then we know where it is in the list of values
        #    Go to the left and right of it appropriate values and recalc the gaps, etc
        # This can probably be a recursive call
    
    
    def _calculate_on_states(self, vals, states, dir=-1):
        # Run twice, once from the front once from the back
        for i in range(0,2):
            if dir >= 0 and dir != i:
                continue
            p = 0 # our pos in the list
            vp = 0 # the index of the value we are working with
            inc = 1 # The directionw e are going
            slen = 0 # the length of the current streak vs the value
            onlen = 0 # the number of cells that have been turned on during this streak
            
            # Start from the back on the second pass
            if i == 1:
                # Start from the back
                p = len(states) - 1
                vp = len(vals) - 1
                inc = -1
                
            # Go until we run out of places to go, regardless of the direction we're going
            while p >= 0 and p < len(states):
                # Move forward until we find a cell that is not off
                if states[p][1] == self.STATE_OFF:
                    # If we were tracking a value, move to the next one
                    if onlen > 0:
                        vp += inc
                    slen = onlen = 0
                # If we're out of values, turn it off and move on to the next cell
                elif vp >= len(vals):
                    states[p][1] = self.STATE_OFF
                # If it is not on, handle it
                elif states[p][1] == self.STATE_UNKNOWN:
                    # If we're still within the allowed range
                    if slen < vals[vp]:
                        # If we have cells that were on, turn this one on
                        # Otherwise we're just going to keep counting available values
                        if onlen > 0:
                            states[p][1] = self.STATE_ON
                            onlen += 1
                        # Increment our length
                        slen += 1
                    # We have reached the end of our available values
                    else:
                        # If we have turned on enough cells to satisfy this value, mark this cell as off
                        if onlen == vals[vp]:
                            states[p][1] = self.STATE_OFF
                            onlen = slen = 0 # start a new streak
                            vp += inc
                        # If we haven't completed the streak, we can't really calculate more values this way
                        else:
                            break
                # ON state
                else:
                    # Increment our streak count and our on count
                    slen += 1
                    onlen += 1
            
                p += inc
                
    def _calculate_overlaps(self, vals, states):
        def next_valid_start(start, needed, inc=1):
            cnt = 0
            loc = p = start
            while cnt < needed:
                #print "%d of %d at %d from %d. max(%d)" % (cnt, needed, p, start, len(states))
                # Out of range
                if p < 0 or p >= len(states):
                    return False
                
                if states[p][1] == self.STATE_OFF:
                    cnt = 0
                    loc = p + inc
                else:
                    cnt += 1
                p += inc
            return loc
            
        def first_possible():
            '''The left-to-right first possible positions for the values'''
            ltor = []
            pos = 0
            for v in vals:
                if v < 1: continue
                pos = next_valid_start(pos, v, 1)
                if pos is False:
                    return False
                pos += v
                ltor.append(pos - 1) # Adjust for 0 vs 1 offset
                pos += 1
            
            return ltor
        
        def last_possible():
            '''The right-to-left first possible positions for the values'''
            rtol = []
            pos = len(states) - 1
            rvals = list(vals)
            rvals.reverse()
            for v in rvals:
                if v < 1: continue
                pos = next_valid_start(pos, v, -1)
                if pos is False:
                    return False
                pos -= v
                rtol.append(pos + 1) # Adjust for 0 vs 1 offset
                pos -= 1
                
            # They're inserted in reverse order, so restore them
            rtol.reverse()
            return rtol
        
        def find_overlaps():
            # Left to right last values
            ltor = first_possible()
            rtol = last_possible()
            
            if ltor is False or rtol is False:
                return False
            
            overlaps = set()
            
            for i in range(0, len(ltor)):
                # If there is an overlap
                if ltor[i] >= rtol[i]:
                    # Calculate from where and for how many squares
                    start = rtol[i]
                    cnt = ltor[i] - rtol[i] + 1
                    overlaps.update(range(start, start+cnt))
            return overlaps
            
        # Calculate overlaps for the rows
        
        overlaps = find_overlaps()
        if overlaps is False:
            return False
        
        for c in overlaps:
            states[c][1] = self.STATE_ON
    
    
    def draw(self, with_headers=True, verbose=False):
        if with_headers:
            # Get the row headers and pad them all properly
            row_headers = [" ".join(str(x) for x in r) for r in self.rows]
            max_len = max([len(h) for h in row_headers])
            for i in range(0, len(row_headers)):
                hlen = len(row_headers[i])
                if hlen < max_len:
                    row_headers[i] = (" " * (max_len-hlen)) + row_headers[i]
                    
            sys.stdout.write("\n")
            self._print_col_headers(max_len + 1)  
        
        for r in range(0, self.height):
            if with_headers:
                sys.stdout.write("%s " % row_headers[r])
            for c in range(0, self.width):
                xy = r*self.width + c
                state = self.cells[xy]
                if state == self.STATE_ON:
                    sys.stdout.write(" # " if with_headers else "# ")
                elif state == self.STATE_OFF:
                    if verbose:
                        sys.stdout.write(" _ " if with_headers else "_ ")
                    else:
                        sys.stdout.write("   " if with_headers else "  ")
                else:
                    if verbose:
                        sys.stdout.write(" . " if with_headers else ". ")
                    else:
                        sys.stdout.write("   " if with_headers else "  ")
            sys.stdout.write("\n")
        sys.stdout.write("\n")
        
        if verbose:
            print "'#' is on, '_' is off, '.' is unknown"
    
    def _print_col_headers(self, lpad = 0):
        cv = 0
        while True:
            row = [" " * lpad]
            hadrow = False
            for c in range(0, self.width):
                if len(self.cols[c]) > cv:
                    hadrow = True
                    row.append("%2d " % self.cols[c][cv])
                else:
                    row.append("   ")
            
            if not hadrow:
                break
            
            cv += 1
            print ''.join(row)
                    