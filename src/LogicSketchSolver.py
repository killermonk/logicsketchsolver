'''
Created on Feb 9, 2014

@author: brian
'''

import sys

def main():
    sys.stdout.write("Width: ")
    width = int(sys.stdin.readline().strip())
    sys.stdout.write("Height: ")
    height = int(sys.stdin.readline().strip())
    
    verbose = False
    for arg in sys.argv:
        arg = arg.lower()
        if arg == '-v' or arg == '--verbose':
            verbose = True
    
    import GameBoard
    board = GameBoard.factory(width, height)
    board.initialize()
    board.solve(100)
    board.draw(with_headers=True, verbose=verbose)


if __name__ == '__main__':
    main()
